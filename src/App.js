import './App.css';
import Menu from './components/Menu'
import Home from './components/Home';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <Menu/>
      <Home/>
      <Footer/>
    </div>
  );
}

export default App;
