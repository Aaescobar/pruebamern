import { Component } from "react"
import { Container, Row, Col, Button, Tabs, Tab, Card, Accordion,Form } from 'react-bootstrap'
import { Formik } from "formik";
import home from '../images/illustration-hero.svg'
import simple from '../images/illustration-features-tab-1.svg'
import speedy from '../images/illustration-features-tab-2.svg'
import easy from '../images/illustration-features-tab-3.svg'
import chrome from '../images/logo-chrome.svg'
import firefox from '../images/logo-firefox.svg'
import opera from '../images/logo-opera.svg'
import line from '../images/bg-dots.svg'
import './Home.css'

export default class Home extends Component {
    render () {
        return (
            <Container>
                <Row className="mt-5 mb-5">
                    <Col xs={{span: 12, order: 'last'}} sm={{span: 12, order: 'last'}} md={{ span: 6, order: 'first' }} style={{marginTop: '20vh', textAlign: 'left'}}>
                        <Container>
                            <h1>A Simple Bookmark Manager</h1>
                            <p>A clean and simple interface to organize your favorite websites. Open a new browser tab and see your  sites load instantly. Try it for free</p>
                            <div className="d-flex gap-2">
                                <Button variant="primary">
                                    Get it on Chrome
                                </Button>
                                <Button variant="secondary">
                                    Get it on Firefox
                                </Button>
                            </div>
                        </Container>
                    </Col>
                    <Col xs={{span: 12, order: 'first'}} sm={{span: 12, order: 'first'}} md={{ span: 6, order: 'last' }}>
                        <div>
                            <img src={home} alt="verga" height={'100%'} width={'100%'}/>                            
                        </div>
                        <div className="backgroundImagesRight">
                        </div>
                    </Col>
                </Row>
                <Row style={{marginTop: '150px'}} className="justify-content-center" id="section1">
                    <Col sm md="8" className="justify-content-center">
                        <Container className="justify-content-center">
                            <h1 className="mb-3">Features</h1>
                            <p>Our aim is to make it quick and easy for you to access your favorite websites. Your booksmarks sync between your divices so you can access them on the go</p>
                        </Container>
                    </Col>
                </Row>
                <Tabs
                    defaultActiveKey="home"
                    id="noanim-tab-example"
                    className="mb-3 justify-content-center"
                    >
                    <Tab eventKey="home" title="Simple Bookmarking">
                        <Row className="mt-5 mb-5">
                            <Col xs={{span: 12}} sm={{span: 12}} md={{ span: 6}} style={{position: 'relative'}}>
                                <div>
                                    <img src={simple} alt="verga" height={'100%'} width={'100%'}/>                            
                                </div>
                                <div className="backgroundImagesLeft">
                                </div>
                            </Col>
                            <Col xs={{span: 12}} sm={{span: 12}} md={{ span: 6 }} style={{marginTop: '10em', textAlign: 'left'}}>
                                <Container>
                                    <h1>Bookmark in one Click</h1>
                                    <p>Organize your booksmarks however you like. Our simple drag and drop gives you complete control over how you manage your favorite sites</p>
                                    <div className="d-flex gap-2">
                                        <Button variant="primary">
                                            More Info
                                        </Button>
                                    </div>
                                </Container>
                            </Col>
                        </Row>
                    </Tab>
                    <Tab eventKey="profile" title="Speedy Searching">
                        <Row className="mt-5 mb-5">
                            <Col xs={{span: 12}} sm={{span: 12}} md={{ span: 6}} style={{position: 'relative'}}>
                                <div>
                                    <img src={speedy} alt="verga" height={'100%'} width={'100%'}/>                            
                                </div>
                                <div className="backgroundImagesLeft">
                                </div>
                            </Col>
                            <Col xs={{span: 12}} sm={{span: 12}} md={{ span: 6 }} style={{marginTop: '10em', textAlign: 'left'}}>
                                <Container>
                                    <h1>Bookmark in one Click</h1>
                                    <p>Organize your booksmarks however you like. Our simple drag and drop gives you complete control over how you manage your favorite sites</p>
                                    <div className="d-flex gap-2">
                                        <Button variant="primary">
                                            More Info
                                        </Button>
                                    </div>
                                </Container>
                            </Col>
                        </Row>
                    </Tab>
                    <Tab eventKey="contact" title="Easy Sharing">
                        <Row className="mt-5 mb-5" >
                            <Col xs={{span: 12}} sm={{span: 12}} md={{ span: 6}} style={{position: 'relative'}}>
                                <div>
                                    <img src={easy} alt="verga" height={'100%'} width={'100%'}/>                            
                                </div>
                                <div className="backgroundImagesLeft">
                                </div>
                            </Col>
                            <Col xs={{span: 12}} sm={{span: 12}} md={{ span: 6 }} style={{marginTop: '10em', textAlign: 'left'}}>
                                <Container>
                                    <h1>Bookmark in one Click</h1>
                                    <p>Organize your booksmarks however you like. Our simple drag and drop gives you complete control over how you manage your favorite sites</p>
                                    <div className="d-flex gap-2">
                                        <Button variant="primary">
                                            More Info
                                        </Button>
                                    </div>
                                </Container>
                            </Col>
                        </Row>  
                    </Tab>
                </Tabs>
                <Row style={{marginTop: '150px'}} className="justify-content-center" id="section2">
                    <Col sm md="8" className="justify-content-center">
                        <Container className="justify-content-center">
                            <h1 className="mb-3">Download the extension</h1>
                            <p>We're got more browsers in the pipeline. Please do let us know if you're got a favourite you'd like us to prioritize</p>
                        </Container>
                    </Col>
                </Row>
                <Row className="justify-content-center mt-5">
                    <Col>
                        <Card style={{ width: '18rem', margin: 'auto' }}>
                            <Card.Img variant="top" src={chrome} style={{padding: '60px', width: '80%', margin: 'auto'}} />
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                <Card.Text>
                                Minimum version 62
                                </Card.Text>
                                <img src={line} alt="verga" height={'100%'}/>                            
                                <Button variant="primary" style={{marginTop: '20px'}}>Add & Install extension</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <Card style={{ width: '18rem', margin: '30px auto 0' }}>
                            <Card.Img variant="top" src={firefox} style={{padding: '60px', width: '80%', margin: 'auto'}}/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                <Card.Text>
                                Minimum version 55
                                </Card.Text>
                                <img src={line} alt="verga" height={'100%'}/>                            
                                <Button variant="primary" style={{marginTop: '20px'}}>Add & Install extension</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <Card style={{ width: '18rem', margin: '60px auto 0' }}>
                            <Card.Img wi src={opera} style={{padding: '60px', width: '80%', margin: 'auto'}}/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                <Card.Text>
                                Minimum version 46
                                </Card.Text>
                                <img src={line} alt="verga" height={'100%'}/>                            
                                <Button variant="primary" style={{marginTop: '20px'}}>Add & Install extension</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <Row style={{marginTop: '150px'}} className="justify-content-center" id="section3">
                    <Col sm md="8" className="justify-content-center">
                        <Container className="justify-content-center">
                            <h1 className="mb-3">Frequently Asked Questions</h1>
                            <p>Here are some of our FAQs. If you have any other questions yoSu'd like answered please feel free to email us</p>
                            <Accordion defaultActiveKey="0" flush>
                                <Accordion.Item eventKey="0">
                                    <Accordion.Header>what is Bookmark?</Accordion.Header>
                                    <Accordion.Body>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                    commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                    est laborum.
                                    </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="1">
                                    <Accordion.Header>How can I request a new browser?</Accordion.Header>
                                    <Accordion.Body>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                    commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                    est laborum.
                                    </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="2">
                                    <Accordion.Header>Is there a mobile app?</Accordion.Header>
                                    <Accordion.Body>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                    commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                    est laborum.
                                    </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="3">
                                    <Accordion.Header>What about other Chromium browsers?</Accordion.Header>
                                    <Accordion.Body>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                    commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                    est laborum.
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                            <Button variant="primary" style={{marginTop: '20px'}}>More Info</Button>
                        </Container>
                    </Col>
                </Row>
                <Row style={{margin: '50px 0', position: "absolute", left: '0', width: '100%', padding: '20px', background: 'hsl(231, 69%, 60%)', color: 'white', height:'50vh'}} className="justify-content-center">
                    <Col sm md="8" className="justify-content-center">
                        <Container className="justify-content-center " style={{width: 'inherit'}}>
                            <p>35.000+ ALREADY JOINED</p>
                            <h2>Stay up-to-date with what we're doing</h2>
                            <Formik
                                initialValues={{
                                email: "",
                                }}
                                validate={(values) => {
                                const errors = {};

                                // We need a valid e-mail
                                if (!values.email) errors.email = "Required";
                                else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email))
                                    errors.email = "Invalid email address";

                                console.log({ values, errors });

                                return errors;
                                }}
                                onSubmit={(values, { setSubmitting }) => {
                                setTimeout(() => {
                                    alert(JSON.stringify(values, null, 2));

                                    setSubmitting(false);
                                }, 250);
                                }}
                            >
                                {(props) => {
                                const {
                                    values,
                                    errors,
                                    touched,
                                    handleChange,
                                    handleBlur,
                                    handleSubmit,
                                    isSubmitting,
                                    /* y otras más */
                                } = props;
                                return (
                                    <Form onSubmit={handleSubmit} className="form-send">
                                        <Form.Group style={{margin: '18px auto'}}>
                                            <Form.Control
                                            style={{ borderColor: errors.email ? 'hsl(0, 94%, 66%)' : "white", color: errors.email ? 'hsl(0, 94%, 66%)' : "white"}}                                            type="email"
                                            name="email"
                                            placeholder="contoso@domain.com"
                                            invalid={errors.email && touched.email}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.email}
                                            />
                                            {
                                                errors.email ? 
                                                <p style={{position: 'absolute', color: 'hsl(0, 94%, 66%)'}}>{errors.email}</p>
                                                :
                                                null
                                            }
                                        </Form.Group>
                                        <Button variant="outline-light" type="submit" disabled={isSubmitting} 
                                        style={{margin: 'auto', borderColor: errors.email ? 'hsl(0, 94%, 66%)' : "white", color: errors.email ? 'hsl(0, 94%, 66%)' : "white" }}>
                                            {isSubmitting ? `Loading` : `Submit`}
                                        </Button>
                                    </Form>
                                );
                                }}
                            </Formik>
                        </Container>
                    </Col>
                </Row>
            </Container>
        )
    }
}