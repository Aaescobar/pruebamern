import { Component } from "react";
import './Menu.css';
import { Link, animateScroll as scroll } from "react-scroll";

import { Navbar, Nav, Container } from 'react-bootstrap'

export default class Footer extends Component {
    scrollToTop = () => {
        scroll.scrollToTop();
    };

    render() {
        return (
            <Navbar bg="dark" className="selec" style={{position: 'relative', bottom: '-59vh', color: 'white'}}>
                <Container className="selec">
                    <Navbar.Brand href="#home" onClick={this.scrollToTop}>
                        <span class="material-icons" style={{ color: 'white', fontSize: '18px', padding: '10px', background: 'purple', borderRadius: '50%' }}>bookmark</span>{'   '}
                        <span style={{ fontWeight: 'bold', color: 'white' }}>BOOKMARK</span>
                    </Navbar.Brand> 
                    <Nav className="selec">
                        <Nav.Link style={{color: 'white'}} href="#features">
                            <Link
                                activeClass="active"
                                to="section1"
                                spy={true}
                                smooth={true}
                                offset={-70}
                                duration={500}
                                >
                                Features
                            </Link>
                        </Nav.Link>
                        <Nav.Link style={{color: 'white'}} href="#pricing">
                            <Link
                                activeClass="active"
                                to="section2"
                                spy={true}
                                smooth={true}
                                offset={-70}
                                duration={500}
                                >
                                Pricing
                            </Link>
                        </Nav.Link>
                        <Nav.Link style={{color: 'white'}} href="#deets">
                            <Link
                                activeClass="active"
                                to="section3"
                                spy={true}
                                smooth={true}
                                offset={-70}
                                duration={500}
                                >
                                Contact
                            </Link>                        
                        </Nav.Link>
                    </Nav>
                    <div>
                        <span className="material-icons icon">facebook</span>{'   '}
                    </div>
                </Container>
            </Navbar>
        )
    }
}