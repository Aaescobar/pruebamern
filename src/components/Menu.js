import { Component } from "react";
import './Menu.css';
import { Link, animateScroll as scroll } from "react-scroll";

import { Navbar, Nav, Container, Offcanvas, Button } from 'react-bootstrap'

export default class Menu extends Component {
    scrollToTop = () => {
        scroll.scrollToTop();
    };

    render() {
        return (
            <Navbar collapseOnSelect expand="md" bg="light">
                <Container>
                    <Navbar.Brand href="#home">
                        <span class="material-icons" style={{ color: 'white', fontSize: '18px', padding: '10px', background: 'purple', borderRadius: '50%' }}>bookmark</span>{'   '}
                        <span style={{ fontWeight: 'bold' }}>BOOKMARK</span>
                    </Navbar.Brand>              
                    <Navbar.Toggle aria-controls="offcanvasNavbar" />
                    <Navbar.Collapse className="justify-content-end">
                        <Nav className="ml-auto">
                            <Nav.Link href="#features">
                                <Link
                                    activeClass="active"
                                    to="section1"
                                    spy={true}
                                    smooth={true}
                                    offset={-70}
                                    duration={500}
                                    >
                                    Features
                                </Link>
                            </Nav.Link>
                            <Nav.Link href="#pricing">
                                <Link
                                    activeClass="active"
                                    to="section2"
                                    spy={true}
                                    smooth={true}
                                    offset={-70}
                                    duration={500}
                                    >
                                    Pricing
                                </Link>
                            </Nav.Link>
                            <Nav.Link href="#deets">
                                <Link
                                    activeClass="active"
                                    to="section3"
                                    spy={true}
                                    smooth={true}
                                    offset={-70}
                                    duration={500}
                                    >
                                    Contact
                                </Link>                        
                            </Nav.Link>
                            <Button variant="warning" className="btn-top-bar" size="sm">LOGIN</Button>
                        </Nav>                
                    </Navbar.Collapse>
                    <Navbar.Offcanvas
                        id="offcanvasNavbar"
                        aria-labelledby="offcanvasNavbarLabel"
                        placement="start"
                        style={{background: '#14161A'}}
                    >
                    <Offcanvas.Header closeButton>
                    <Offcanvas.Title id="offcanvasNavbarLabel" style={{textAlign: 'center', color: 'white'}}>
                        <span class="material-icons" style={{color: 'white', fontSize: '18px', padding: '10px', background: 'purple', borderRadius: '50%' }}>bookmark</span>{'   '}
                        <span>BOOKMARK</span>
                    </Offcanvas.Title>
                    </Offcanvas.Header>
                    <Offcanvas.Body>
                    <Nav className="justify-content-center flex-grow-1 pe-3">
                        <Nav.Link href="#action1" style={{textAlign: 'center', color: 'white'}} >Features</Nav.Link>
                        <Nav.Link href="#action2" style={{textAlign: 'center', color: 'white'}} >Pricing</Nav.Link>
                        <Nav.Link href="#action2" style={{textAlign: 'center', color: 'white'}} >Contact</Nav.Link>
                        <Button variant="outline-light" >LOGIN</Button>
                    </Nav>
                    </Offcanvas.Body>
                    </Navbar.Offcanvas>
                </Container>
            </Navbar>
        )
    }
}
